import SwiftUI
import PlaygroundSupport


// 1. Implicit Return Types //
func printString(features: String) -> String {
    features
}
printString(features: "I'm an Implicit Return Type")

struct ContentView: View {
    var body: some View {
        return Text("Hello, World!")
    }
}


// 2. Opaque Return Types //
func double(num: Int) -> Int {
    num * 2
}
double(num: 2)

func opaqueDouble(num: Int) -> some Equatable {
    num * 2
}
opaqueDouble(num: 3)

struct SomeViewOpaqueType: View {
    var body: some View {
        Circle()
    }
}


// 3. Method Chaining in View Modifiers //
struct ViewWithModifiers: View {
    var body: some View {
        Circle()
            .foregroundColor(.green)
            .frame(width: 100, height: 100, alignment: .center)
    }
}

PlaygroundPage.current.setLiveView(ViewWithModifiers())


// 4. Trailing Closures and Function Builders in Container Views //
struct ContainerView: View {
    var body: some View {
        HStack {
            Text("Hello, World!")
            Circle()
        }
    }
}


// 5. Property Wrappers //
@propertyWrapper struct Lowercased {
    private var text: String
    
    var wrappedValue: String {
        get { text.lowercased() }
        set { text = newValue }
    }
    
    init(wrappedValue: String) {
        self.text = wrappedValue
    }
}

struct ProfileView: View {
    @Lowercased var email: String
    
    var body: some View {
        Text(email)
    }
}

struct MainView: View {
    var body: some View {
        ProfileView(email: "john.smith@avenuecode.com")
    }
}

PlaygroundPage.current.setLiveView(MainView())
